

1 . Within application\config\constants.php add the following lines:

````
defined('VENDORPATH') OR define('VENDORPATH', FCPATH.'vendor');``
`````

2. Create composer.json file to Project Root and  add the following lines
````
{
    "repositories": [
        {
            "type": "vcs",
            "url" : "https://{USER_NAME}@bitbucket.org/{USER_NAME}/{PACKAGE_NAME}.git"

        }
    ],
    "require": {
        "{USER_NAME}/{PACKAGE_NAME}": "dev-master"
    }
}
````

3. Run ``composer update`` Command run in Project root.

3. Run ``composer require xeroapi/xero-php-oauth2`` Command run in Project root.

3. Create a file application\controllers\Xero_connection.php with the following content:

````
<?php
if (is_file(VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\Xero_connection.php')) {
    require_once VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\Xero_connection.php';
}
?>
````

4. Within  Existing autoload helper in  add the following lines:

````
<?php
if (is_file(VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\helpers\xero_helper.php')) {
    require_once VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\helpers\xero_helper.php';
}
?>
````

5. Create a file application\libraries\Xero.php with the following content:

````
<?php
if (is_file(VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\libraries\Xero.php')) {
    require_once VENDORPATH.'\{USER_NAME}\{PACKAGE_NAME}\libraries\Xero.php';
}
?>
````


6. Within application\config\autoload.php add the following lines:

````
<?php
$autoload['libraries'] = array('xero');
?>
````

7.  Within application\config\config.php add the following lines:

`````
/*
|--------------------------------------------------------------------------
| Xero Setting Key
|--------------------------------------------------------------------------
|
*/

$config['clientId'] = '8BF3030CE98844938291859FCF546174';
$config['clientSecret'] = 'O7owAqJBUkZKDXzJ7vlgwRiIBuP5e2WrqXKotBwVOWfZa_6w';
$config['redirectUri'] = '{PROJECT_PATH}/xero_connection/xero_callback';
`````

-> Above redirectUri add xero account when create credential for xero api.


7. Run this url {PROJECT_PATH}/Xero_connection.

