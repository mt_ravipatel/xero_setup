<?php
class StorageClass
{
	public $xero_cache_file = CURR_DIR . 'application/cache/xero.cache';
	function __construct() {
		/*if( !isset($_SESSION) ){
        	$this->init_session();
    	}*/
   	}

   	public function init_session(){
    	///session_start();
	}

    public function getSession() {
    	if (is_file($this->xero_cache_file)) {
	    	$fp = file_get_contents($this->xero_cache_file);
	    	$oauth2 = unserialize($fp);
	    	return $oauth2;
    	}
    }

 	public function startSession($token, $secret, $expires = null){
       	//session_start();
	}

	public function setToken($token, $expires = null, $tenantId,$refreshToken, $idToken){    
	    $oauth2 = [
	        'token' => $token,
	        'expires' => $expires,
	        'tenant_id' => $tenantId,
	        'refresh_token' => $refreshToken,
	        'id_token' => $idToken
	    ];
	    $oauth2 = serialize($oauth2);
	    $fp = fopen($this->xero_cache_file, 'w');
		fwrite($fp, $oauth2);
		fclose($fp);
	}

	public function getToken(){
		if (is_file($this->xero_cache_file)) {
		    //If it doesn't exist or is expired, return null
		    if (!empty($this->getSession())
		        || ($this->getSession()['expires'] !== null
		        && $this->getSession()['expires'] <= time())
		    ) {
		        return null;
		    }
		    return $this->getSession();			
		}
	}

	public function getAccessToken(){
		if (is_file($this->xero_cache_file)) {
	    	return $this->getSession()['token'];
		}
	}

	public function getRefreshToken(){
		if (is_file($this->xero_cache_file)) {
	    	return $this->getSession()['refresh_token'];
		}
	}

	public function getExpires(){
		if (is_file($this->xero_cache_file)) {
	    	return $this->getSession()['expires'];
		}
	}

	public function getXeroTenantId(){
		if (is_file($this->xero_cache_file)) {
	    	return $this->getSession()['tenant_id'];
		}
	}

	public function getIdToken(){
		if (is_file($this->xero_cache_file)) {
	    	return $this->getSession()['id_token'];
		}
	}

	public function getHasExpired(){

		if (is_file($this->xero_cache_file)) {
			if (!empty($this->getSession())){
				if(time() > $this->getExpires()){
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}	
		}
	}
}
?>