    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 'On');
//include_once APPPATH.'/third_party/xero/authorization.php';
require VENDORPATH . '\autoload.php';
require_once(VENDORPATH.'\mt_ravipatel/xero_setup/xero_storage.php');
  // Use this class to deserialize error caught
use XeroAPI\XeroPHP\AccountingObjectSerializer;
class Xero {

    public $apiInstance;
    public $organisation;
    public $xeroTenantId;
    
    public function __construct()
    {

        if(DISABLE_XERO){
            return false;
        }

        $storage = new StorageClass();
        $ci &= get_instance();
        //$config = new ConfigClass(); 
         $this->xeroTenantId = $xeroTenantId =  $storage->getSession()['tenant_id'];

        if ($storage->getHasExpired()) {
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $ci->config->item('clientId'),   
            'clientSecret'            => $ci->config->item('clientSecret'),
            'redirectUri'             => $ci->config->item('redirectUri'),
            'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken'          => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);

        $newAccessToken = $provider->getAccessToken('refresh_token', [
          'refresh_token' => $storage->getRefreshToken()
        ]);

            // Save my token, expiration and refresh token
        $storage->setToken(
                $newAccessToken->getToken(),
                $newAccessToken->getExpires(),
                $xeroTenantId,
                $newAccessToken->getRefreshToken(),
                $newAccessToken->getValues()["id_token"] );
        }

        $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
        $config->setHost("https://api.xero.com/api.xro/2.0");

        $this->apiInstance = $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
          new GuzzleHttp\Client(),
          $config
        );
        
    }
}